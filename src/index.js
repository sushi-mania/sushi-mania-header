import React from 'react';

import { CartLink } from '@sushi-mania/cart';
import { StyledHeader, StyledLink } from './styles';

export default function Header() {
  return (
    <StyledHeader>
      <StyledLink to="/">
        <h1>
          <span role="img" aria-label="sushi">
            🍣
          </span>{' '}
          Sushi Mania
        </h1>
      </StyledLink>
      <CartLink />
    </StyledHeader>
  );
}
